import java.util.*;

class Program10{
	public static void main(String [] s){
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter String = ");
		String str = sc.nextLine();
		
		int n = str.length();
		
		if(str.isEmpty()){
			System.out.println("String is empty");
		}
		else{
			System.out.println(str.charAt(n-1));
		}
	}
}

