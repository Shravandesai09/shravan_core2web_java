import java.io.*;

class Program9{
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter rows :");
        int row = Integer.parseInt(br.readLine());

        System.out.println("Enter colums :");
        int col = Integer.parseInt(br.readLine());

        int arr[][] = new int[row][col];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.print("Enter Element in row " + (i + 1) + " and column " + (j + 1) + " : ");
                arr[i][j] = Integer.parseInt(br.readLine());
            }
        }

        int product = 1;
        int sumPrimary = 0;
        int sumSecondary = 0;
        int n = arr.length;
        for (int i = 0; i < arr.length; i++) {
            sumSecondary += arr[i][n-i-1];
            for(int j = 0 ; j < arr[0].length ; j++){
                if(i == j){
                    sumPrimary += arr[i][j];
                }
            }
        }
        product = sumSecondary * sumPrimary;
        System.out.println("Product of Sum of Primary and Secondary Diagonal is : "+ product);
    }
}
