import java.io.*;

class Program5{
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter rows :");
        int row = Integer.parseInt(br.readLine());

        System.out.println("Enter colums :");
        int col = Integer.parseInt(br.readLine());

        int arr[][] = new int[row][col];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.print("Enter Element in row " + (i + 1) + "  and column " + (j + 1) + " : ");
                arr[i][j] = Integer.parseInt(br.readLine());
            }
        }

        for (int j = 0; j < arr[0].length; j++) {
            int sum = 0;
            for (int i = 0; i < arr.length; i++) {
                sum += arr[i][j];
            }
            System.out.println("Sum of Column " + (j + 1) + " is :" + sum);

        }
    }

}
