import java.util.*;

class Program6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enters Rows :");
		int row = sc.nextInt();
		int col = 0;
		int col2 = 0;
		for(int i = 1 ; i <= (2 * row) - 1 ; i++){
			
			if(i <= row){
				col = i;	
				col2  = row - i;		
			}
			else{					
				col = (2 * row) - i;
				col2 = i - row;
			}
			
			for(int sp = 1 ; sp <= col2 ; sp++){
                                System.out.print(" " + "\t");
                        }
			
			for(int j = 1 ; j <= col ; j++){
				System.out.print(j + "\t");
			}
			
			System.out.println();
		}
	}
}
