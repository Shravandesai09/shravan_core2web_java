import java.util.*;

class Program2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enters Rows :");
		int row = sc.nextInt();
		int col = 0;
		
		for(int i = 1 ; i <= (2 * row) - 1 ; i++){
			int num = 1;
			if(i <= row){
				col = i;
			}
			else{					
				col = (2 * row) - i;
			}
			for(int j = 1 ; j <= col ; j++){
				System.out.print(num + "\t");
				num++;
			}
			System.out.println();
		}
	}
}
