import java.util.*;

class Program9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enters Rows :");
		int row = sc.nextInt();
		int col = 0;
		int col2 = 0;
		int num = 0;
		for(int i = 1 ; i <= (2 * row) - 1 ; i++){
			
			if(i <= row){
				col = i;	
				col2  = row - i;		
				num = row - i;
			}

			else{					
				col = (2 * row) - i;
				col2 = i - row;
				num =  i - row;
			}
			
			for(int sp = 1 ; sp <= col2 ; sp++){
                                System.out.print(" " + "\t");
                        }
			
			for(int j = 1 ; j <= col ; j++){
				System.out.print(num + "\t");
				num++;
			}
			
			System.out.println();
		}
	}
}
