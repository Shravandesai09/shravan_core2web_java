import java.util.*;

class Program10{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enters Rows :");
        int row = sc.nextInt();
        int col = 0;
        int space = 0;
        for (int i = 1; i <= (2 * row) - 1; i++) {
            int num = 1;
            if (i <= row) {
                col = (2 * i) - 1;
                space = row - i;
            } else {
                col -= 2;
                space = i - row;
            }

            for (int sp = 1; sp <= space; sp++) {
                System.out.print(" \t");
            }

            for (int j = 1; j <= col; j++) {
                if (i <= row) {
                    if (j % 2 != 0) {

                        if ((j >= i)) {
                            System.out.print(num + "\t");
                            num--;
                        } else {
                            System.out.print(num + "\t");
                            num++;
                        }
                    } else {
                        System.out.print("A\t");
                    }
                } else {
                    if ((j < (2 * row) - i)) {
                        System.out.print(num + "\t");
                        num++;
                    } else {
                        System.out.print(num + "\t");
                        num--;
                    }
                }
            }
            System.out.println();
        }
    }
}
