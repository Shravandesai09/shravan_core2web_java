import java.io.*;

class Program5{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
                System.out.print("Enter Rows : ");
                int row = Integer.parseInt(br.readLine());
                int num = (row * row) + row;
                for(int i = 1; i <= row ; i++){
			char ch = 'A';
			char ch1 = 'a';
                        for(int j = i ; j <= row ; j++){
				if(i % 2 != 0){
					System.out.print(ch + " ");
					ch++;
				}
				else{
					System.out.print(ch1 + " ");
                                        ch1++;
                                }
			}
			System.out.println();
		}
	}
}
