import java.io.*;

class Program4{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
                System.out.print("Enter Rows : ");
                int row = Integer.parseInt(br.readLine());
                int ch;
		if(row % 2 == 0){
			 ch = 65 + row + row + 1;
		}
		else{
			 ch = 65 + row + row - 1;
		}

                for(int i = 1; i <= row ; i++){
                        for(int j = i ; j <= row ; j++){
				System.out.print((char)ch + " ");
				ch--;
			}
			System.out.println();
		}
	}
}

