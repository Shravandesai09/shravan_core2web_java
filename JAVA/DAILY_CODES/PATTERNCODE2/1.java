import java.io.*;

class Program1{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
                System.out.print("Enter Rows : ");
                int row = Integer.parseInt(br.readLine());
                int num = (row * row) + row;
                for(int i = 1; i <= row ; i++){
                        for(int j = i ; j <= row ; j++){
				System.out.print(j + " ");
			}
			System.out.println();
		}
	}
}
