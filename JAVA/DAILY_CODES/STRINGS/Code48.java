class DiffDemo2{
	public static void main(String[] s){
		String str1 = "Rahul";
		String str2 = "Rahul";
		
		System.out.println(str1.equals(str2)); // contents
		System.out.println(str1 == str2); // address
		
		String str3 = "Ashish";
		String str4 = new String("Ashish");
		
		System.out.println(str3.equals(str4)); // contents
		System.out.println(str3 == str4); // address

		
	}
}

