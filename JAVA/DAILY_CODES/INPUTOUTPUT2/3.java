import java.util.*;

class ScannerDemo3{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);

                System.out.println("Enter Name: ");
                String name = sc.next();

                System.out.println("Enter College Name: ");
                String clgName = sc.next();

                System.out.println("Enter Student ID: ");
                int Id = sc.nextInt();

                System.out.println("Enter CGPA  : ");
                float cgpa = sc.nextFloat();

		System.out.println("Name : " + name);
		System.out.println("College Name : " + clgName);
		System.out.println("ID : " + Id);
		System.out.println("CGPA : " + cgpa);
        }
}
