class GeeksForGeeks {
    
    public static void main(String[] args){
        int n=6;
	int i,j;
        //rows
        for (i = 1; i <= n; i++) {
            //columns(spaces)
            for (j = 1; j <= n-i ; j++) {
                System.out.print(" " + " ");
            }
            //columns(star)
            for (j = 1; j <= 2*i-1 ; j++) {
                System.out.print("*" + " ");
            }
            //new line
            System.out.println();
        }

        //revese
         for (i = n; i >= 1; i--) {
            //columns(spaces)
            for (j = 1; j <= n-i ; j++) {
                System.out.print(" " + " ");
            }
            //columns(star)
            for (j = 1; j <= 2*i-1 ; j++) {
                System.out.print("*" + " ");
            }
            //new line
            System.out.println();
        }
    }
 
    
   }
